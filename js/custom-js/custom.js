/*jslint browser: true*/ /*global  $*/
'use strict';

$(function(){
    console.log("Custom JS added!");
    
    // Double click the copyright bar to add some spicy Comic Sans to the website
    // Please remove this before going to production
    $('.copyright-bar').dblclick(function(){
        $('*').css('font-family', 'Comic Sans MS', '!important');
    });

    // Get correct Google Maps height
    const maps = $('#google-maps-maps iframe');
    const textHeight = () => {
        return $('#google-maps-text')[0].clientHeight;
    };
    const setGoogleMapsHeight = () => {
        maps.css('height', textHeight(), '!important');
        // console.log(`Height adjusted to ${textHeight()}`);
    };

    // Set correct Google Maps height on load
    setGoogleMapsHeight();

    // On window resize, set correct Google Maps height
    $(window).resize(function(){ setGoogleMapsHeight() });
});
