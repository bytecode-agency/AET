<?php

/*
|----------------------------------------------------
|  Framework: setup of the custom functions
|----------------------------------------------------
*/

require_once("inc/cleanup.php"); // Cleanup of the admin area
require_once("inc/custom-theme.php"); // Adds custom features to the website
require_once("inc/disable-comments.php"); // Disables comments site-wide

define( 'DISALLOW_FILE_EDIT', true ); //Disable theme and plugin editors
add_filter( 'jetpack_development_mode', '__return_true' ); // Sets Jetpack to dev mode
//add_action( 'admin_menu', 'gp_remove_menus' ); // Remove menu's for Dev -> Production
//add_action( 'admin_bar_menu', 'remove_some_nodes_from_admin_top_bar_menu', 999 ); // Remove customizer option from top bar
//add_action( 'admin_menu', 'disable_customizer' ); // Remove customizer from wp-admin
//add_action( 'wp_head', 'add_google_analytics');

/*************** CUSTOM PHP BELOW ***************/

define('CONCATENATE_SCRIPTS', false);
